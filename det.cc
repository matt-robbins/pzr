#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <cstddef>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

#include "Classify.pb.h"
#include <zmq.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/program_options.hpp>

extern "C" {
    #include <radd/sound_buffer.h>
    #include <radd/detector.h>
    #include <samplerate.h>
}

using namespace std;
using namespace classify;

int64_t samples_to_time(int samples, int rate)
{
    return ((1000000 * (int64_t) samples) / rate);
}

int run_detector(detector_t * det, zmq::socket_t &in, zmq::socket_t &out)
{
    int rate = det->sound_setup.rate;
    int buflen = det->sound_setup.N;
    int padlen = buflen * (det->sound_setup.buffer_history + det->sound_setup.buffer_latency);
        
    zmq::message_t message;
    classify::AudioData data;
    classify::Event event;

    struct timespec t;
    int64_t time;
    
    int * buf = new int[buflen];
    event_t * events = new event_t[128];
    
    /* grab first message */
    in.recv(&message);
    data.ParseFromString(
        std::string(static_cast<char*>(message.data()), message.size()));
        
    int packet_N = data.samples_size();
    int packet_rate = data.rate();
    
    std::cout << "size: " << packet_N << " rate: " << packet_rate << endl;
    
    int N = packet_N;

    float * raw_buffer = 0;
    float * resample = 0;
    SRC_DATA src;

    if (rate != packet_rate)
    {
        raw_buffer = new float[packet_N];
        double r = (double) rate / (double) packet_rate;
        int rN = (int) ((float) packet_N * (float) r);
        resample = new float[rN];
        src.data_in = raw_buffer; src.input_frames = packet_N;
        src.data_out = resample; src.output_frames = rN;
        src.src_ratio = r;
        N = rN;
    }
    
    boost::circular_buffer<float> cb(buflen + padlen + N);

    while (1)
    {
        while (cb.size() < buflen + padlen)
        {
            in.recv(&message);
            data.ParseFromString(
                std::string(static_cast<char*>(message.data()), message.size()));
            
            if (!resample)
            {
                for (int i = 0; i < data.samples_size(); i++)
                    cb.push_back(data.samples(i));
            }
            else
            {
                for (int i = 0; i < data.samples_size(); i++)
                    raw_buffer[i] = data.samples(i);
                
                src_simple(&src, SRC_SINC_MEDIUM_QUALITY, 1);
                
                for (int i = 0; i < src.output_frames_gen; i++)
                    cb.push_back(resample[i]);  
            }
            
            time = data.time();
        }
                
        while (cb.size() >= buflen + padlen)
        {
            for (int i = 0; i < buflen; i++)
            {
                buf[i] = (int) ((float) cb[padlen + i] * (float) INT_MAX);
            }
            
            int64_t offset = cb.size() - padlen;
            static int64_t last_buftime = 0;
            int64_t buftime = time - samples_to_time(offset, rate);
            
            last_buftime = buftime; 
            
            int n_events = detector_run_buffer(det, events, buf, buflen, buftime);
            
            for (int i = 0; i < n_events; i++)
            {
                std::cout << "event with score " << events[i].score << std::endl;
                                                
                if (padlen + events[i].t_in < 0 || 
                    padlen + events[i].t_in + events[i].t_dt > cb.size())
                {
                    std::cout << "event outside buffer limits!" << endl;
                    continue;
                }              
                
                std::cout << "hi!" << std::endl;
                char * det_str = (char *) events[i].data;
                std::cout << det_str << std::endl;

                event.set_source_id(data.source_id());
                event.set_detector(det_str);
                event.set_time(buftime + samples_to_time(events[i].t_in, rate));
                event.set_duration((float) samples_to_time(events[i].t_dt, rate) / 1000000.0);
                event.set_low_freq(events[i].f_lo);
                event.set_high_freq(events[i].f_lo + events[i].f_df);
                
                classify::AudioData * d = event.mutable_data();

                d->clear_samples();
                
                for (int j = 0; j < events[i].t_dt; j++)
                {
                    d->add_samples(cb[padlen+events[i].t_in+j]);
                }
                
                d->set_source_id(data.source_id());
                d->set_rate(rate);
                
                string ser_event;
                event.SerializeToString(&ser_event);
                
                zmq::message_t event_msg (ser_event.length());
                memcpy (event_msg.data(), ser_event.c_str(), ser_event.length());
                
                try {
                    out.send (event_msg);
                }
                catch (zmq::error_t err)
                {
                    std::cerr << "error!!!" << std::endl;
                }
            }
            
            for (int i = 0; i < buflen; i++)
                cb.pop_front();
        }
    }
    
    delete buf;
    delete events;
    
    return 0;
}


int main(int argc, char * argv[])
{
    string detname;
    string path;
    namespace po = boost::program_options;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("source-url,i", po::value<string>()->default_value("tcp://localhost:5555"), "source url")
        ("dest-url,o", po::value<string>()->default_value("tcp://localhost:5556"),"destination url")
        ("detector-name,d", po::value<string>(&detname), "detector name")
        ("detector-path,p", po::value<string>(&path)->default_value("/usr/lib/detectors"), "detector path")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help") || vm.count("detector-name") == 0) {
        cout << desc << endl;
        return 1;
    }
        
    zmq::context_t context (1);
    zmq::socket_t out (context, ZMQ_PUSH);
    out.connect(vm["dest-url"].as<string>().c_str());
    
    zmq::socket_t in (context, ZMQ_SUB);
    in.connect(vm["source-url"].as<string>().c_str());
    
    in.setsockopt(ZMQ_SUBSCRIBE,NULL,0);
        
    detector_t * det = detector_create((char *)path.c_str(), (char *)detname.c_str());
    
    if (!det)
        return 1;
        
    detector_initialize(det, NULL);
    
    run_detector(det, in, out);
        
    zmq_close (in);
    zmq_close (out);
    zmq_ctx_destroy (context);
    return 0;
}
