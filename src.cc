#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <cstddef>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>

#include "Classify.pb.h"
#include <zmq.hpp>
#include <boost/program_options.hpp>

using namespace std;
using namespace classify;

namespace po = boost::program_options;

int64_t timespec_to_time(struct timespec t)
{
    return (int64_t) t.tv_sec * 1000000 + t.tv_nsec / 1000;
}

int64_t timespec_diff(struct timespec t1, struct timespec t2)
{
    int64_t tt1 = timespec_to_time(t1);
    int64_t tt2 = timespec_to_time(t2);
    
    return (tt1 - tt2);
}

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}


int main(int argc, char * argv[])
{
    int port, rate;
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("port,p", po::value<int>()->default_value(5555), "port to bind to")
        ("rate,r", po::value<int>(&rate)->default_value(44100),"sample rate")
        ("source-name,N", po::value<string>()->default_value("my_source"), "Source Name")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    
    std::cout << "port: " << vm["port"].as<int>() << std::endl;
    
    classify::AudioData data;
    struct timespec t1, t2;
    int64_t t;
    static int64_t lt = 0;
    
    zmq::context_t context (1);
    zmq::socket_t sender (context, ZMQ_PUB);
    
    char addr[128];
    sprintf(addr, "tcp://*:%d", vm["port"].as<int>());
    
    sender.bind(addr);
    
    int buflen = 4000;
    
    int *buf = new int[buflen];
    
    int64_t buftime = (int64_t) buflen * 1000000 / rate;
    
    string packet;
    
    while(1)
    {
        clock_gettime(CLOCK_REALTIME, &t2);
        cin.read((char *)buf,buflen * sizeof(int));
        clock_gettime(CLOCK_REALTIME, &t1);

        if (!cin)
        {
            sleep(1);
            std::cerr << "failed to read packet from stdin, " << cin.gcount() << " read." << std::endl;
        }
        
        t += buftime;
        
        if (timespec_diff(t1, t2) > buftime/2)
        {
            int64_t nt = timespec_to_time(t1);
            
            if (abs((int)(nt - t)) < buftime)
            {
                int64_t td = (2 * (nt > t) - 1);
                t += td;
            }
            else
            {
                t = nt;
            }
        }
        else
            lt = t;
            
        data.set_source_id(vm["source-name"].as<string>());
        data.set_rate(rate);
        data.set_time((uint64_t) t);
        data.clear_samples();
        
        for (int i = 0; i < buflen; i++)
        {
            data.add_samples((float) buf[i] / (float)INT_MAX); 
        }
        
        data.SerializeToString(&packet);
        
        zmq::message_t msg (packet.length());
        memcpy (msg.data(), packet.c_str(), packet.length());
        
        try {
            sender.send (msg);
        }
        catch (zmq::error_t err)
        {
            std::cerr << "error!!!" << std::endl;
        }
    }
}
