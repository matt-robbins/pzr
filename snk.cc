#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <cstddef>
#include <time.h>
#include <sys/types.h>

#include "Classify.pb.h"
#include <zmq.hpp>

#include <sndfile.h>

using namespace std;
using namespace classify;

int main(int argc, char * argv[])
{
    classify::Event event;
    struct timespec t;
    string ser_event;
    
    zmq::context_t context (1);
    zmq::socket_t receiver (context, ZMQ_PULL);
    receiver.bind("tcp://*:5556");
    
    SF_INFO info;
    SNDFILE * fp;
    
    int i = 0;
    
    while(1)
    {
        zmq::message_t message;
        receiver.recv(&message);
        std::string msg = std::string(static_cast<char*>(message.data()), message.size());
        
        event.ParseFromString(msg);
        
        std::cout << "event detector = " << event.detector() << std::endl;
        
        char fname[128];
        
        sprintf(fname, "%d.wav", i++);
        info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
        info.channels = 1;
        info.samplerate = event.data().rate();
        
        fp = sf_open(fname, SFM_WRITE, &info);
        if (fp)
        {            
            for (int i = 0; i < event.data().samples_size(); i++)
            {
                float s = event.data().samples(i);
                int n = sf_writef_float(fp, &s, 1);
            }
            sf_close(fp);
        }
    }
}
