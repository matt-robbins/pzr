#!/usr/bin/python

import Classify_pb2
import cffi
import zmq

import sqlite3
import os
import base64
import requests
import datetime
import pysndfile

from bson import json_util
import json
import numpy
import tempfile

context = zmq.Context()

sink = context.socket(zmq.PULL)
sink.bind("tcp://*:7778")

event = Classify_pb2.Event()

url = 'http://deedee01.ornith.cornell.edu/packet_sink/'

while True:
    msg = sink.recv()
    
    event.ParseFromString(msg)
    print "got event!"
    print event.data.rate
    print len(event.data.samples)
    try:            
        f = tempfile.NamedTemporaryFile()
        
        pysndfile.sndio.write(
            f.name, numpy.array(event.data.samples), 
            event.data.rate, format='wav', enc='pcm16')
            
        f.seek(0)
        b64 = base64.encodestring(f.read());
        
        f.close()
    except Exception as e:
        continue

    packet = {'hwid': event.source_id,
		'time': datetime.datetime.utcnow(),
		'events': [],
		'status':[],
		'spectra':[]}
        
    e = {}    
    e['clipdata'] = b64
    e['time'] = datetime.datetime.utcfromtimestamp(event.time/1000000.)
    print e['time']
    e['duration'] = event.duration
    e['frequency'] = event.low_freq
    e['bandwidth'] = event.high_freq - event.low_freq
    e['score'] = 5
    e['author'] = event.detector
    e['data'] = event.detector
    
    packet['remote_dt'] = datetime.datetime.utcnow()
    packet['events'] = [e]
    packet['bytes'] = len(json.dumps(packet,default=json_util.default))
    
#    js = json.dumps(packet,default=json_util.default)
#    r = requests.post(url, {'method': 'post_packet','params': js,'id': 1})
#    
#    result = json.loads(r.text)
#    print result    
    
    
        
    
