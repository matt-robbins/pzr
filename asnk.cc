#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <cstddef>
#include <time.h>
#include <sys/types.h>

#include "Classify.pb.h"
#include <zmq.hpp>
#include <boost/program_options.hpp>

using namespace std;
using namespace classify;

int main(int argc, char * argv[])
{
    string detname;
    string path;
    namespace po = boost::program_options;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("source-url,i", po::value<string>()->default_value("tcp://localhost:5555"), "source url")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        cout << desc << endl;
        return 1;
    }
        
    zmq::context_t context (1);

    zmq::socket_t in (context, ZMQ_SUB);
    in.connect(vm["source-url"].as<string>().c_str());
    
    in.setsockopt(ZMQ_SUBSCRIBE,NULL,0);
    
    zmq::message_t message;
    classify::AudioData data;

    while (1)
    {
        in.recv(&message);
        data.ParseFromString(
            std::string(static_cast<char*>(message.data()), message.size()));
        
        for (int i = 0; i < data.samples_size(); i++)
        {
            int16_t sample = (int16_t) (data.samples(i) * 65535);
            fwrite(&sample, sizeof(int16_t),1,stdout);
        }
    }
        
        
    zmq_close (in);
    zmq_ctx_destroy (context);
    return 0;
}
