
CXX=g++
LIBS=-lprotobuf -lsndfile -lzmq -lradd -lsamplerate -lboost_program_options
PROTOC=protoc

DET_SRCS = det.cc
SRC_SRCS = src.cc
SNK_SRCS = snk.cc
ASNK_SRCS = asnk.cc

PB_SRCS = Classify.pb.cc
PB_OBJS = $(PB_SRCS:.cc=.o)

DET_OBJS=$(DET_SRCS:.cc=.o)

%.pb.h %.pb.cc: %.proto
	protoc --cpp_out=. --python_out=. $<

%.o: %.cc
	$(CXX) -c -o $@ $< $(CFLAGS)

all: det src snk asnk Makefile

det: $(DET_SRCS) $(PB_OBJS)
	$(CXX) -o $@ $< $(PB_OBJS) $(LIBS)

src: $(SRC_SRCS) $(PB_OBJS)
	$(CXX) -o $@ $< $(PB_OBJS) $(LIBS)

snk: $(SNK_SRCS) $(PB_OBJS)
	$(CXX) -o $@ $< $(PB_OBJS) $(LIBS)
	
asnk: $(ASNK_SRCS) $(PB_OBJS)
	$(CXX) -o $@ $< $(PB_OBJS) $(LIBS)

clean:
	rm det src snk
	rm *.o
	rm *.pb.*
	rm *_pb2.py
