#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <cstddef>
#include <time.h>
#include <sys/types.h>

#include "Classify.pb.h"
#include <zmq.hpp>
#include <boost/program_options.hpp>

extern "C" {
    #include <radd/classifier.h>
    #include <samplerate.h>
}


using namespace std;
using namespace classify;

int main(int argc, char * argv[])
{
    namespace po = boost::program_options;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("source-url,i", po::value<string>()->default_value("tcp://localhost:5555"), "source url")
        ("dest-url,o", po::value<string>()->default_value("tcp://localhost:5556"),"destination url")
        ("classifier-name,d", po::value<string>(&detname), "classifier name")
        ("classifier-path,p", po::value<string>(&path)->default_value("/var/lib/classifiers"), "classifier path")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help") || vm.count("detector-name") == 0) {
        cout << desc << endl;
        return 1;
    }
    
    classify::Event event;
    struct timespec t;
    string ser_event;
    
    zmq::context_t context (1);
    zmq::socket_t receiver (context, ZMQ_PULL);
    zmq::socket_t sender (context, ZMQ_PUSH);
    receiver.connect("tcp://localhost:5557");
    sender.connect("tcp://localhost:5558");
    while(1)
    {
        zmq::message_t message;
        receiver.recv(&message);
        std::string msg = std::string(static_cast<char*>(message.data()), message.size());
        
        event.ParseFromString(msg);
        
        std::cout << "event detector = " << event.detector() << std::endl;
    }
}
